***How to run SenGen***

* gradle clean build
* ./gradlew bootRun
*

***OR***

Run previously builded executable jar in this repo, "SenGen-0.0.1.jar"

Link to postman workspace:
[POSTMAN SENGEN](https://go.postman.co/workspace/col-be~b30cda7c-e76b-48e1-92f3-4b2f60d20f9b/collection/12889693-2784b507-1adf-41d4-8d25-dbea7140a94c)