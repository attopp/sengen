package com.gooddata.entrytest.sengen.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gooddata.entrytest.sengen.dto.SentenceDto;
import com.gooddata.entrytest.sengen.dto.WordDto;
import com.gooddata.entrytest.sengen.service.WordService;
import com.gooddata.entrytest.sengen.util.TestCategory;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@ExtendWith(MockitoExtension.class)
@Tag(TestCategory.UNIT_TEST)
@WebMvcTest(WordController.class)
public class WordControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private WordService wordService;

    @Test
    public void Resource_should_be_running()
            throws Exception {
        mvc.perform(get("/words/ping").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
    }

    @Test
    public void Adding_new_word_should_return_success()
            throws Exception {

        WordDto word = new WordDto();
        word.setValue("Test");
        word.setCategory("NOUN");

        mvc.perform(post("/words")
                        .content(objectMapper.writeValueAsString(word))
                        .contentType("application/json"))
                .andExpect(status().isOk());
    }

    @Test
    public void Adding_new_word_should_fail()
            throws Exception {
        SentenceDto sentenceDto = new SentenceDto();


        mvc.perform(post("/words")
                        .content(objectMapper.writeValueAsString(sentenceDto))
                        .contentType("application/json"))
                .andExpect(status().is(400));
    }

}
