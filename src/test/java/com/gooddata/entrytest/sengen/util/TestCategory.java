package com.gooddata.entrytest.sengen.util;

public final class TestCategory {
    public static final String INTEGRATION_TEST = "IntegrationTest";
    public static final String UNIT_TEST = "UnitTest";
}

