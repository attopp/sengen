package com.gooddata.entrytest.sengen.service;

import com.gooddata.entrytest.sengen.dto.SentenceDto;
import com.gooddata.entrytest.sengen.mapper.SentenceMapper;
import com.gooddata.entrytest.sengen.model.Sentence;
import com.gooddata.entrytest.sengen.model.Word;
import com.gooddata.entrytest.sengen.model.enums.WordCategory;
import com.gooddata.entrytest.sengen.repository.SentenceRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
@RequiredArgsConstructor
public class SentenceServiceImpl implements SentenceService {
    private final SentenceRepository sentenceRepository;
    private final SentenceMapper sentenceMapper;
    private final WordService wordService;


    @Override
    public List<SentenceDto> listAllSentences() {
        List<Sentence> sentences = StreamSupport.stream(sentenceRepository.findAll().spliterator(), false)
                .collect(Collectors.toList());
        return sentenceMapper.toDto(sentences);
    }

    @Override
    public SentenceDto findSentenceById(Integer id, boolean yodaTalks) {
        Sentence sentence = sentenceRepository.findById(id).orElseThrow();
        sentence.setViews(sentence.getViews() + 1);
        sentenceRepository.save(sentence);
        return yodaTalks ? sentenceMapper.toDtoYoda(sentenceRepository.findById(id).orElseThrow()) : sentenceMapper.toDto(sentenceRepository.findById(id).orElseThrow());
    }

    @Override
    public void generateSentence() {
        Word noun = wordService.findRandomByWordType(WordCategory.NOUN).orElseThrow();
        Word verb = wordService.findRandomByWordType(WordCategory.VERB).orElseThrow();
        Word adjective = wordService.findRandomByWordType(WordCategory.ADJECTIVE).orElseThrow();

        Sentence sentence = new Sentence();
        sentence.setNoun(noun);
        sentence.setVerb(verb);
        sentence.setAdjective(adjective);
        sentenceRepository.save(sentence);
    }
}
