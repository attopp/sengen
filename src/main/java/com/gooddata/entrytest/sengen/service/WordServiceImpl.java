package com.gooddata.entrytest.sengen.service;

import com.gooddata.entrytest.sengen.dto.WordDto;
import com.gooddata.entrytest.sengen.mapper.WordMapper;
import com.gooddata.entrytest.sengen.model.Word;
import com.gooddata.entrytest.sengen.model.enums.WordCategory;
import com.gooddata.entrytest.sengen.repository.WordRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
@RequiredArgsConstructor
public class WordServiceImpl implements WordService {
    private final WordRepository wordRepository;
    private final WordMapper wordMapper;

    @Override
    public List<WordDto> listAllWords() {
        return wordMapper.toDto((List<Word>) wordRepository.findAll());
    }

    @Override
    public void saveWord(WordDto word) {
        wordRepository.save(wordMapper.fromDto(word));
    }

    @Override
    public void editWord(WordDto wordDto) {
        Word word = wordRepository.findById(wordDto.getId()).orElseThrow();
        word.setCategory(WordCategory.valueOf(wordDto.getCategory()));
        wordRepository.save(word);
    }

    @Override
    public WordDto findWordById(Integer id) {
        return wordMapper.toDto(wordRepository.findById(id).orElseThrow());
    }

    @Override
    public Optional<Word> findRandomByWordType(WordCategory category) {
        List<Word> wordsByCategory = StreamSupport.stream(wordRepository.findByWordType(category).spliterator(), false)
                .collect(Collectors.toList());
        int randomNum = ThreadLocalRandom.current().nextInt(0, wordsByCategory.size());
        return Optional.of(wordsByCategory.get(randomNum));
    }

}
