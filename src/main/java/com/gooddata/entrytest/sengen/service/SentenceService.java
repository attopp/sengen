package com.gooddata.entrytest.sengen.service;

import com.gooddata.entrytest.sengen.dto.SentenceDto;

import java.util.List;

public interface SentenceService {
    List<SentenceDto> listAllSentences();

    SentenceDto findSentenceById(Integer id, boolean yoda);

    void generateSentence();
}
