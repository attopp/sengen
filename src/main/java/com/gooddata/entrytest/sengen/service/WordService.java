package com.gooddata.entrytest.sengen.service;

import com.gooddata.entrytest.sengen.dto.WordDto;
import com.gooddata.entrytest.sengen.model.Word;
import com.gooddata.entrytest.sengen.model.enums.WordCategory;

import java.util.List;
import java.util.Optional;

public interface WordService {
    List<WordDto> listAllWords();

    void saveWord(WordDto word);

    void editWord(WordDto word);

    WordDto findWordById(Integer id);

    Optional<Word> findRandomByWordType(WordCategory category);
}
