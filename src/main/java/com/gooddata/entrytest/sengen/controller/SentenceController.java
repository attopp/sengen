package com.gooddata.entrytest.sengen.controller;

import com.gooddata.entrytest.sengen.dto.SentenceDto;
import com.gooddata.entrytest.sengen.service.SentenceService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("sentences")
public class SentenceController {

    private final SentenceService sentenceService;

    @GetMapping(value = {"/ping"})
    public ResponseEntity<String> ping() {
        return ResponseEntity.ok("SenGen 'sentence' resource up and running!");
    }

    @GetMapping(value = {""})
    public ResponseEntity<List<SentenceDto>> listAllSentences() {
        List<SentenceDto> sentences = sentenceService.listAllSentences();
        return ResponseEntity.ok(sentences);
    }

    @PostMapping(value = {"/generate"})
    public ResponseEntity<?> generateSentence() {
        sentenceService.generateSentence();
        return ResponseEntity.ok().build();
    }

    @GetMapping(value = {"/{id}"})
    public ResponseEntity<SentenceDto> findSentenceById(@PathVariable Integer id) {
        SentenceDto sentence = sentenceService.findSentenceById(id, false);
        return ResponseEntity.ok(sentence);
    }

    @GetMapping(value = {"/{id}/yoda"})
    public ResponseEntity<SentenceDto> findSentenceByIdYodaVersion(@PathVariable Integer id) {
        SentenceDto sentence = sentenceService.findSentenceById(id, true);
        return ResponseEntity.ok(sentence);
    }

}
