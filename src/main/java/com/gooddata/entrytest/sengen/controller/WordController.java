package com.gooddata.entrytest.sengen.controller;

import com.gooddata.entrytest.sengen.dto.WordDto;
import com.gooddata.entrytest.sengen.service.WordService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@RestController
@RequiredArgsConstructor
@RequestMapping("words")
@Validated
public class WordController {

    private final WordService wordService;

    @GetMapping(value = {"/ping"})
    public ResponseEntity<String> ping() {
        return ResponseEntity.ok("SenGen 'words' resource up and running!");
    }

    @GetMapping(value = {""})
    public ResponseEntity<List<WordDto>> listAllWords() {
        List<WordDto> words = wordService.listAllWords();
        return ResponseEntity.ok(words);
    }

    @PostMapping(value = {""})
    public ResponseEntity<?> addWord(@Valid @RequestBody WordDto word) {
        /* try {}catch ()*/
        wordService.saveWord(word);
        return ResponseEntity.ok().build();
    }

    @PutMapping(value = {""})
    public ResponseEntity<?> editWord(@Valid @RequestBody WordDto word) {
        wordService.editWord(word);
        return ResponseEntity.ok().build();
    }

    @GetMapping(value = {"/{id}"})
    public ResponseEntity<WordDto> findWordById(@PathVariable Integer id) {
        WordDto word = wordService.findWordById(id);
        return ResponseEntity.ok(word);
    }

}
