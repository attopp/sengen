package com.gooddata.entrytest.sengen.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@EqualsAndHashCode(callSuper = true)
@RequiredArgsConstructor
@AllArgsConstructor
@Entity
public class Sentence extends AuditingEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "noun_id", referencedColumnName = "id")
    private Word noun;

    @ManyToOne
    @JoinColumn(name = "verb_id", referencedColumnName = "id")
    private Word verb;

    @ManyToOne
    @JoinColumn(name = "adjective_id", referencedColumnName = "id")
    private Word adjective;

    private Integer views = 0;

}