package com.gooddata.entrytest.sengen.model.enums;

public enum WordCategory {
    NOUN, VERB, ADJECTIVE;

    public String getName() {
        return name();
    }
}
