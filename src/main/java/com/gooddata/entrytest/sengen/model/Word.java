package com.gooddata.entrytest.sengen.model;

import com.gooddata.entrytest.sengen.model.enums.WordCategory;
import com.sun.istack.NotNull;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@EqualsAndHashCode(callSuper = true)
@Data
@RequiredArgsConstructor
@Entity
public class Word extends AuditingEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @NotNull
    @Column(name = "w_value")
    private String value;

    @Column(name = "w_category")
    @Enumerated(EnumType.STRING)
    private WordCategory category;
}
