package com.gooddata.entrytest.sengen.mapper;

import com.gooddata.entrytest.sengen.dto.AuditingDto;
import com.gooddata.entrytest.sengen.model.AuditingEntity;
import org.mapstruct.MapperConfig;


@MapperConfig(componentModel = "spring")
public interface AuditingEntityMapper {

    AuditingDto toDto(AuditingEntity entity);

    AuditingEntity fromDto(AuditingDto dTo);

}
