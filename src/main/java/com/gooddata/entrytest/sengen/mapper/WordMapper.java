package com.gooddata.entrytest.sengen.mapper;

import com.gooddata.entrytest.sengen.dto.WordDto;
import com.gooddata.entrytest.sengen.model.Word;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.List;

@Mapper(componentModel = "spring", uses = AuditingEntityMapper.class)
public interface WordMapper {
    WordDto toDto(Word entity);

    List<WordDto> toDto(List<Word> entities);

    @Mapping(expression = "java(dTo.getValue().toLowerCase().replaceAll(\"[^a-zA-Z]+\",\"\"))", target = "value")
    @Named(value = "toDto")
    Word fromDto(WordDto dTo);

}
