package com.gooddata.entrytest.sengen.mapper;

import com.gooddata.entrytest.sengen.dto.SentenceDto;
import com.gooddata.entrytest.sengen.model.Sentence;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.List;

@Mapper(componentModel = "spring", uses = AuditingEntityMapper.class)
public interface SentenceMapper {

    @Mapping(expression = "java(entity.getAdjective().getValue() + ' ' + entity.getNoun().getValue() +" +
            " ' ' + entity.getVerb().getValue())", target = "value")
    @Named(value = "toDtoYoda")
    SentenceDto toDtoYoda(Sentence entity);

    @Mapping(expression = "java(entity.getNoun().getValue() + ' ' + entity.getVerb().getValue() + " +
            "' ' + entity.getAdjective().getValue())", target = "value")
    @Named(value = "toDto")
    SentenceDto toDto(Sentence entity);

    @IterableMapping(qualifiedByName = "toDto")
    List<SentenceDto> toDto(List<Sentence> entities);


    Sentence fromDto(SentenceDto dTo);


}
