package com.gooddata.entrytest.sengen.exception;

import lombok.Data;
import org.springframework.http.HttpStatus;

import java.util.Arrays;
import java.util.List;

@Data
public class SenGenApiErr {

    private HttpStatus status;
    private String message;
    private List<String> errors;

    public SenGenApiErr() {
        super();
    }

    public SenGenApiErr(final HttpStatus status, final String message, final List<String> errors) {
        super();
        this.status = status;
        this.message = message;
        this.errors = errors;
    }

    public SenGenApiErr(final HttpStatus status, final String message, final String error) {
        super();
        this.status = status;
        this.message = message;
        errors = Arrays.asList(error);
    }
}
