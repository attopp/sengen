package com.gooddata.entrytest.sengen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SenGenApplication {

    public static void main(String[] args) {
        SpringApplication.run(SenGenApplication.class, args);
    }

}
