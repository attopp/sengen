package com.gooddata.entrytest.sengen.repository;

import com.gooddata.entrytest.sengen.model.Sentence;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SentenceRepository extends CrudRepository<Sentence, Integer> {


}