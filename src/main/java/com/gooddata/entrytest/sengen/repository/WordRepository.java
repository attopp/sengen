package com.gooddata.entrytest.sengen.repository;

import com.gooddata.entrytest.sengen.model.Word;
import com.gooddata.entrytest.sengen.model.enums.WordCategory;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WordRepository extends CrudRepository<Word, Integer> {

    @Query("from Word w where w.category=:category")
    public Iterable<Word> findByWordType(WordCategory category);

}
