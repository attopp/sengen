package com.gooddata.entrytest.sengen.dto;


import lombok.Data;

import java.time.LocalDateTime;

@Data
public abstract class AuditingDto {
    private LocalDateTime createdDate;
    private LocalDateTime lastModifiedDate;
}
