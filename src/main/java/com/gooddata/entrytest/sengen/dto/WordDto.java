package com.gooddata.entrytest.sengen.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;

@EqualsAndHashCode(callSuper = true)
@Data
public class WordDto extends AuditingDto {

    private Integer id;

    @NotNull
    private String value;

    @NotNull
    private String category;
}
