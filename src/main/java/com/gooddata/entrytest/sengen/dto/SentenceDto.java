package com.gooddata.entrytest.sengen.dto;


import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class SentenceDto extends AuditingDto {
    private Integer id;
    private WordDto noun;
    private WordDto verb;
    private WordDto adjective;
    private Integer views;
    private String value;

}
