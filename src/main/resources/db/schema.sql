create
database sengen

create table "Word"
(
    id         integer     not null
        constraint word_pk
            primary key,
    w_value    varchar(50) not null,
    w_category varchar(50) not null
);

create table "Sentence"
(
    id           integer not null
        constraint sentence_pk
            primary key,
    s_value      varchar(250),
    noun_id      integer not null
        constraint sentence_word_noun_fk
            references "Word",
    verb_id      integer
        constraint sentence_word_verb_fk
            references "Word",
    objective_id integer
        constraint sentence_word_objective_fk
            references "Word"
);


